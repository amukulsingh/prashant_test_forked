package com.online.transfer;

public class BookTransferException extends RuntimeException {
    public BookTransferException() {
    	super();
    }
    public BookTransferException(String s) {
    	super(s);
    }
    public BookTransferException(String s, Throwable throwable) {
    	super(s, throwable);
    }
    public BookTransferException(Throwable throwable) {
    	super(throwable);
    }
}
